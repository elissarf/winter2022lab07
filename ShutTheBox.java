public class ShutTheBox {
	public static void main(String[] args) {
		System.out.println("WLECOME TO GAME");
		Board b = new Board();
		boolean gameOver = false;

		// switching players until there is game over
		while (!gameOver) {
			System.out.println("Player 1's turn");

			gameOver = b.playATurn();
			System.out.println(b);

			if (gameOver) {
				System.out.println("Player 2 wins");
			} else {
				System.out.println("Player 2's turn");

				gameOver = b.playATurn();
				System.out.println(b);
				if (gameOver) {
					System.out.println("Player 1 wins");
				}
			}

		}
	}
}