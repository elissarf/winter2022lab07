import java.util.Random;

public class Die {
	private int pips;
	private Random random;

	// constructer
	public Die() {
		pips = 1;
		random = new Random();
	}

	// get method for pips
	public int getPips() {
		return this.pips;
	}

	// method to roll random number
	// it is going to be from 1 to 6
	public void roll() {
		this.pips = random.nextInt(6) + 1;
	}

	// toString method
	public String toString() {
		return "" + pips;
	}
}