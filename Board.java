public class Board {
	private Die roll1;
	private Die roll2;
	private int[][] closedTiles;

	// constructer
	public Board() {
		roll1 = new Die();
		roll2 = new Die();
		closedTiles = new int[6][6];
	}

	// toString method
	public String toString() {
		String boardName = "";
		for (int i = 0; i < closedTiles.length; i++) {
			for (int j = 0; j < closedTiles.length; j++) {
				boardName = boardName + closedTiles[i][j] + " ";
			}
			boardName = boardName + "\n";
		}
		return boardName;
	}

	// method to roll the dice and increment the tiles
	public boolean playATurn() {
		final int thirdRoll= 3;

		roll1.roll();
		roll2.roll();

		System.out.println("first roll is " + roll1 + " and second roll is " + roll2);

		closedTiles[roll1.getPips() - 1][roll2.getPips() - 1]++;

		if (closedTiles[roll1.getPips() - 1][roll2.getPips() - 1] == thirdRoll) {
			System.out.println("the tile " + roll1.getPips() + "," + roll2.getPips() + " is used thrice already");
			return true;
		} else {
			System.out.println("the tile " + roll1.getPips() + "," + roll2.getPips() + " is icremented");
			return false;
		}
	}
}
